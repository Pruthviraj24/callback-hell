/*
    Folder structure:
        ├── problem1.js
        ├── problem2.js
        └── test
            ├── testProblem1.js
            └── testProblem2.js
*/

/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/



const fs =  require("fs");

const problem1 = ()=>{

    fs.mkdir("random",(error)=>{
        if(error){
            console.log(error)
        }else{
            fs.writeFile("./random/file.json", "{'example':'data'}", (error)=>{
                if(error){
                    console.log(error)
                }else{
                    fs.unlink("./random/file.json",(error)=>{
                        if(error){
                            console.log(error);
                        }else{
                            console.log("Completed creating random folder and JSON file inside it and deleting it simultaneously");
                        }
                    })
                }
            })
        }
    });
}

exports.module = problem1;