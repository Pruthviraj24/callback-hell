/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");

function problem2(){

fs.readFile("lipsum.txt","utf8",(error,data)=>{
    if(error){
        console.log(error)
    }else{
       const lipsumData =  data.toString().toUpperCase();
        const newFile1 = "upperCaseLipsumData.txt"
       fs.appendFile(`${newFile1}`,lipsumData,(error)=>{
        if(error){
            console.log(error)
        }else{
            fs.appendFile("filenames.txt",`${newFile1}`,(error)=>{
                if(error){
                    console.log(error)
                }else{
                    fs.readFile(`${newFile1}`,"utf8",(error,data)=>{
                        if(error){
                            console.log(error)
                        }else{
                            const newFileData = data.toString().toLowerCase().split(".").toString();
                            const newFile2 = "lowerCaseData.txt"
                            fs.appendFile(`${newFile2}`,newFileData,(error)=>{
                                if(error){
                                    console.log(error)
                                }else{
                                    fs.appendFile("filenames.txt",`-${newFile2}`,(error)=>{
                                        if(error){
                                            console.log(error)
                                        }else{
                                            fs.readFile(`${newFile1}`, "utf8",(error,data)=>{
                                                if(error){
                                                    console.log(error)
                                                }else{
                                                    let newFilesData = data.toString()
                                                    fs.readFile(`${newFile2}`, "utf8",(error,data2)=>{
                                                         if(error){
                                                            console.log(error)
                                                         }else{
                                                            newFilesData += data2.toString()
                                                            const sortedData = newFilesData.split(' ').join('').split('').sort().join('')
                                                            const newFile3 = "concatedData"
                                                            fs.appendFile(`${newFile3}`,sortedData,(error)=>{
                                                                if(error){
                                                                    console.log(error)
                                                                }else{
                                                                    fs.appendFile("filenames.txt", `-${newFile3}`,(error)=>{
                                                                        if(error){
                                                                            console.log(error)
                                                                        }else{
                                                                            fs.readFile("filenames.txt","utf8",(error,fileNamesData)=>{
                                                                                if(error){
                                                                                    console.log(error)
                                                                                }else{
                                                                                    const arrayOfFileNames = fileNamesData.split("-")
                                                                                    for(let fileName of arrayOfFileNames){
                                                                                        fs.unlink(fileName,(error)=>{
                                                                                            if(error){
                                                                                                console.log(error);
                                                                                            }else{
                                                                                                console.log(`Deleted file ${fileName}`)
                                                                                            }
                                                                                        })
                                                                                    }
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                         }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
       })
    }
})

}

exports.module = problem2;
